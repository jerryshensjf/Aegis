package org.light.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.InterVarUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class FindByName extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
					+ "\" parameterType=\"string\" resultMap=\"" + this.domain.getLowerFirstDomainName() + "\">"));
			list.add(new Statement(200L, 2, MybatisOracleSqlReflector.generateFindByNameStatement(domain)));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setReturnType(
					new Type(this.domain.getCapFirstDomainNameWithSuffix(), this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.getDomainName().getFieldName(), new Type("String")));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setReturnType(
					new Type(this.domain.getCapFirstDomainNameWithSuffix(), this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.getDomainName().getFieldName(), new Type("String")));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(
					"find" + this.domain.getStandardName() + "By" + this.domain.getDomainName().getCapFirstFieldName());
			method.setReturnType(
					new Type(this.domain.getCapFirstDomainNameWithSuffix(), this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.addSignature(new Signature(1, this.domain.getDomainName().getFieldName(), new Type("String")));
			method.setThrowException(true);
			method.addMetaData("Override");

			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(1000L, 2, "return "
					+ daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName()) + ";"));
			method.setMethodStatementList(WriteableUtil.merge(list));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public FindByName() {
		super();
		if (this.domain != null)
			this.setVerbName("Find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getStandardName() + "Name"));
		else
			this.setVerbName("FindByName");
		this.setLabel("根据名称查找");
	}

	public FindByName(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("FindByName");
		this.setVerbName("Find" + this.domain.getStandardName() + "By"
				+ StringUtil.capFirst(this.domain.getStandardName() + "Name"));
		this.setLabel("根据名称查找");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("FindByName");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(
					"find" + this.domain.getStandardName() + "By" + this.domain.getDomainName().getCapFirstFieldName());
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.addSignature(new Signature(1, this.domain.getDomainName().getLowerFirstFieldName(),
					this.domain.getDomainName().getFieldRawType(), "", "RequestParam(value = \""
							+ this.domain.getDomainName().getLowerFirstFieldName() + "\", required = true)"));
			method.addMetaData("RequestMapping(value = \"/" + StringUtil.lowerFirst(method.getStandardName())
					+ "\", method = RequestMethod.POST)");

			List<Writeable> wlist = new ArrayList<Writeable>();
			Var resultMap = new Var("result", new Type("TreeMap<String,Object>", "java.util"));
			Var domainVar = new Var(this.domain.getLowerFirstDomainName(),
					new Type(this.domain.getCapFirstDomainName(), this.domain.getPackageToken() + "."
							+ this.domain.getDomainSuffix() + "." + this.domain.getCapFirstDomainNameWithSuffix()));
			Var service = new Var("service",
					new Type(this.domain.getStandardName() + "Service", this.domain.getPackageToken()));
			Method serviceMethod = this.generateServiceMethodDefinition();
			wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
			wlist.add(NamedStatementGenerator.getSpringMVCCallServiceMethodByDomainNameReturnDomain(2000L, 2,
					this.domain, service, serviceMethod));
			wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndDomainVar(3000L, 2, resultMap,
					domainVar));
			wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName() + ";"));
			method.setMethodStatementList(WriteableUtil.merge(wlist));

			return method;
		}
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return null;
	}
}
