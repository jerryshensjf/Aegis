package org.light.domain;

import java.util.List;
import java.util.Set;

import org.light.exception.ValidateException;
import org.light.utils.StringUtil;

public class Dropdown extends Field implements Comparable<Object>  {
	protected String aliasName;
	protected String targetName;
	protected Domain target;

	public Set<String> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(Set<String> annotations) {
		this.annotations = annotations;
	}
	
	public void addAnnotation(String annotation){
		this.annotations.add(annotation);
	}

	public void setFieldType(Type fieldType) {
		this.fieldType = fieldType;
	}


	public String getFieldComment() {
		return fieldComment;
	}

	public void setFieldComment(String fieldComment) {
		this.fieldComment = fieldComment;
	}

	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Dropdown(String targetName){
		super();
		this.targetName = targetName;
	}
	
	public void decorate(Domain domain) throws ValidateException{
		if (StringUtil.isBlank(domain)) throw new ValidateException("目标域对象设置错误！","error");
		else if (StringUtil.isBlank(this.targetName)) throw new ValidateException("未设置目标域对象名称！","error");
		this.target = domain;
		if (StringUtil.isBlank(this.aliasName)) {
			this.aliasName = this.target.getLowerFirstDomainName() +"Id";
		}		
		Field f = (Field)domain.getDomainId().clone();
		f.setFieldName(this.aliasName);
		this.setFieldName(f.getFieldName());
		this.setFieldType(f.getFieldType());
		this.setFieldComment(f.getFieldComment());
		this.setFieldValue(f.getFieldValue());
		this.setTokens(f.getTokens());
		this.setAnnotations(f.getAnnotations());
		this.setLabel(f.getLabel());
	}
	
	public JavascriptMethod generateTranslateMethod(){
		JavascriptMethod method = new JavascriptMethod();
		method.setStandardName("translate"+this.target.getCapFirstDomainName());
		Signature s1 = new Signature();
		s1.setName("value");
		s1.setPosition(1);
		s1.setType(new Type("var"));	
		method.addSignature(s1);
		
		StatementList sl = new StatementList();
		sl.add(new Statement(500,1, "if (isBlank(value)) return \"\";"));
		sl.add(new Statement(1000,1, "var retVal = value;"));
		sl.add(new Statement(2000,1, "$.ajax({"));
		sl.add(new Statement(3000,2, "type:\"post\","));
		sl.add(new Statement(4000,2, "url: \"../"+this.target.getLowerFirstDomainName()+this.target.getControllerNamingSuffix()+"/find"+this.target.getCapFirstDomainName()+"By"+this.target.getDomainId().getCapFirstFieldName()+"\","));
		sl.add(new Statement(5000,2, "data: {"));
		sl.add(new Statement(6000,3, this.target.getDomainId().getLowerFirstFieldName()+":value"));
		sl.add(new Statement(7000,2, "},"));
		sl.add(new Statement(8000,2, "dataType: 'json',"));
		sl.add(new Statement(9000,2, "async:false,"));
		sl.add(new Statement(10000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(11000,3, "debugger;"));
		sl.add(new Statement(11200,3, "if ( data.data !=null && data.data."+this.target.getDomainName().getLowerFirstFieldName()+"!=null){"));
		sl.add(new Statement(11400,4, "retVal = data.data."+this.target.getDomainName().getLowerFirstFieldName()+";"));
		sl.add(new Statement(11600,3, "}else{"));
		sl.add(new Statement(11800,4, "retVal = \"\";"));
		sl.add(new Statement(12000,3, "}"));
		sl.add(new Statement(13000,2, "},"));
		sl.add(new Statement(14000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(15000,2, "},"));
		sl.add(new Statement(16000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(17000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(18000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(19000,2, "}"));
		sl.add(new Statement(20000,1, "});"));
		sl.add(new Statement(21000,1, "return retVal;"));
		
		method.setMethodStatementList(sl);
		return method;	
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public Domain getTarget() {
		return target;
	}

	public void setTarget(Domain target) {
		this.target = target;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	
	public String getForeignKeyAlias(){
		return this.target.getLowerFirstDomainName()+"Id";		
	}
}
