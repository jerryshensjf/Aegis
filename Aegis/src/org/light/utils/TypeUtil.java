package org.light.utils;

import org.light.domain.Domain;
import org.light.domain.Type;

public class TypeUtil {
	public static boolean isNumeric(Type type){
		if (type.getTypeName().equals("Integer")||type.getTypeName().equals("int")||type.getTypeName().equalsIgnoreCase("long")||type.getTypeName().equalsIgnoreCase("float")||
				type.getTypeName().equalsIgnoreCase("double")||type.getTypeName().equalsIgnoreCase("byte")||type.getTypeName().equalsIgnoreCase("short")||type.getTypeName().equals("BigDecimal")){
			return true;
		}else {
			return false;		
		}		
	}
	
	public static class Collection {
		public static class List {
			public static Type getListInstance(Domain d){
				if (d != null)
					return new Type("List", d, "java.util");
				else
					return new Type("List", "java.util");
			}
		}
		
		public static class ArrayList {
			public static Type getListInstance(Domain d){
				if (d != null)
					return new Type("ArrayList", d, "java.util");
				else
					return new Type("ArrayList", "java.util");
			}
		}
		
		public static class Set {
			public static Type getSetInstance(Domain d){
				if (d != null)
					return new Type("Set", d,"java.util");
				else
					return new Type("Set","java.util");
			}
		}
	}
}
