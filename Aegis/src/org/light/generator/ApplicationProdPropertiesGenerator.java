package org.light.generator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.IndependentConfig;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class ApplicationProdPropertiesGenerator extends IndependentConfig{
	private static final long serialVersionUID = -3330296613142657974L;
	protected String projectName;
	

	public ApplicationProdPropertiesGenerator(){
		super();
		this.fileName = "application-prod.properties";
		this.standardName = "applicationProdProperties";
		this.folder = "src/main/resources/";
	}
	
	@Override
	public String generateImplString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"server.port=8080"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"project.name="+this.projectName));
		sList.add(new Statement(4000L,0,"project.time="+new SimpleDateFormat("yyyy-MM-dd").format(new Date())));
		return WriteableUtil.merge(sList).getContent();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
