package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.light.core.Writeable;
import org.light.domain.Independent;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class IndexControllerJavaGenerator extends Independent{
	private static final long serialVersionUID = -731157618972753321L;

	public IndexControllerJavaGenerator(){
		super();
		this.fileName = "IndexController.java";
		this.standardName = "IndexController";
	}
	
	public IndexControllerJavaGenerator(String packageToken){
		super();
		this.packageToken = packageToken;
		this.fileName = "IndexController.java";
		this.standardName = "IndexController";
	}
	
	@Override
	public void setPackageToken(String packageToken){
		this.packageToken = packageToken+"";
	}
	
	@Override
	public String generateImplString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"package "+this.packageToken+";"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import org.springframework.stereotype.Controller;"));
		sList.add(new Statement(4000L,0,"import org.springframework.web.bind.annotation.RequestMapping;"));
		sList.add(new Statement(5000L,0,""));
		sList.add(new Statement(6000L,0,"@Controller"));
		sList.add(new Statement(7000L,0,"public class IndexController {"));
		sList.add(new Statement(8000L,1,"@RequestMapping(\"/\")"));
		sList.add(new Statement(9000L,1,"public String index(){"));
		sList.add(new Statement(10000L,2,"return \"index.html\";"));
		sList.add(new Statement(11000L,1,"}"));
		sList.add(new Statement(12000L,0,"}"));
		return WriteableUtil.merge(sList).getContent();
	}
}
