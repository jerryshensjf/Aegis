package org.light.include;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Include;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;

public class Footer extends Include{
	protected String company = "Mind Rules";
	
	public Footer(){
		super();
		this.fileName = "footer.jsp";
		this.packageToken = "";
	}
	
	public Footer(String company){
		super();
		this.fileName = "footer.jsp";
		this.packageToken = "";
		this.company = company;
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!-- Common footer for our site -->");
		sb.append("<div id=\"footer_wide\">");
		sb.append("\t<p id=\"legal\">Copyright &copy; 2014  "+this.company+". Author:jerry_shen_sjf@qq.com  QQ group:277689737<br/>");
		sb.append("\t</p>");
		sb.append("</div>");
		return sb.toString();
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,indent,"<!-- Common footer for our site -->"));
		list.add(new Statement(2000L,indent,"<div id=\"footer_wide\">"));
		list.add(new Statement(3000L,indent+1,"<p id=\"legal\">Copyright &copy; 2014  "+this.company+". Author:jerry_shen_sjf@qq.com  QQ group:277689737<br/>"));
		list.add(new Statement(5000L,indent+1,"</p>"));
		list.add(new Statement(6000L,indent,"</div>"));
		StatementList myList = WriteableUtil.merge(list);
		myList.setSerial(serial);
		return myList;
	}
}
