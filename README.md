# 宙斯盾SpringCloud代码生成器

#### 简介
一个为SpringCloud微服务架构定制的Java代码生成器。

### 技术架构
SpringCloud H

SpringBoot 2.2.2

SpringMVC

Spring

MyBatis

MariaDB/Oracle

界面采用EasyUI


### 项目图片
![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/205039_ce11e9e9_1203742.jpeg "aegos.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/212926_f0473177_1203742.jpeg "zs2.jpg")

### 项目进展
代码从第三代动词算子式代码生成器：光的基础上分支出来。

### Excel代码生成界面
![输入图片说明](https://images.gitee.com/uploads/images/2020/1107/130751_df48a9fd_1203742.png "aegis.png")